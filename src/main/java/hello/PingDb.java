package hello;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.CouchbaseCluster;
import com.couchbase.client.java.document.JsonDocument;
import com.couchbase.client.java.document.json.JsonArray;
import com.couchbase.client.java.document.json.JsonObject;
import com.couchbase.client.java.query.N1qlQuery;
import com.couchbase.client.java.query.N1qlQueryResult;
import com.couchbase.client.java.query.N1qlQueryRow;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.Date;

@SpringBootApplication
@EnableScheduling
public class PingDb implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(PingDb.class);

    public static void main(String args[]) {
        SpringApplication.run(PingDb.class, args);
    }

    @Override
    public void run(String... strings) throws Exception {

//        Cluster cluster = CouchbaseCluster.create("localhost");
//        cluster.authenticate("admin", "password");
//        Bucket bucket = cluster.openBucket("taxfree-accounting");
//
//        N1qlQueryResult result = bucket.query(
//                N1qlQuery.parameterized("select * from `taxfree-accounting` where type='admin'"
//                        ,JsonArray.from(""))
//        );
//
//        // Print each found Row
//        for (N1qlQueryRow row : result) {
//            System.out.println(row);
//        }


    }

    @Scheduled(fixedRate = 50000)
    public void ping() {
        System.out.println("AAAAAAAAAaa");
        Cluster cluster = CouchbaseCluster.create("localhost");
        cluster.authenticate("admin", "password");
        Bucket bucket = cluster.openBucket("taxfree-accounting");

//        N1qlQueryResult result = bucket.query(
//                N1qlQuery.parameterized("select * from `taxfree-accounting` where type='admin'"
//                        ,JsonArray.from(""))
//        );

        JsonObject arthur = JsonObject.create()
                .put("name", "Arthur")
                .put("email", "kingarthur@couchbase.com")
                .put("date", new Date().getTime())
                .put("interests", JsonArray.from("Holy Grail", "African Swallows"));

        // Store the Document
        bucket.upsert(JsonDocument.create("u:king_arthur", arthur));

        // Load the Document and print it
        // Prints Content and Metadata of the stored Document
//        System.out.println(bucket.get("u:king_arthur"));

        N1qlQueryResult result = bucket.query(
                N1qlQuery.parameterized("select timestamp from `taxfree` where timestamp=1553011307878"
                        ,JsonArray.from(""))
        );

        // Print each found Row
        for (N1qlQueryRow row : result) {
            System.out.println(row.value().get("timestamp").getClass());
            System.out.println(row);
        }
    }
}
