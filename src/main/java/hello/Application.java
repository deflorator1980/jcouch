//package hello;
//
//import com.couchbase.client.java.Bucket;
//import com.couchbase.client.java.Cluster;
//import com.couchbase.client.java.CouchbaseCluster;
//import com.couchbase.client.java.document.json.JsonArray;
//import com.couchbase.client.java.query.N1qlQuery;
//import com.couchbase.client.java.query.N1qlQueryResult;
//import com.couchbase.client.java.query.N1qlQueryRow;
//import com.github.couchmove.Couchmove;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
//
//@SpringBootApplication
//public class Application implements CommandLineRunner {
//
//    private static final Logger log = LoggerFactory.getLogger(Application.class);
//
//    public static void main(String args[]) {
//        SpringApplication.run(Application.class, args);
//    }
//
//    @Override
//    public void run(String... strings) throws Exception {
//
//        Cluster cluster = CouchbaseCluster.create("localhost");
//        cluster.authenticate("h", "hhhhhh");
//        Bucket bucket = cluster.openBucket("my");
//
//        // Store the Document
//        bucket.upsert(JsonDocument.create("u:king_arthur", arthur));
//
//        // Load the Document and print it
//        // Prints Content and Metadata of the stored Document
//        System.out.println(bucket.get("u:king_arthur"));
//
//
////        // Create a JSON Document
////        JsonObject arthur = JsonObject.create()
////            .put("name", "Arthur")
////            .put("email", "kingarthur@couchbase.com")
////            .put("interests", JsonArray.from("Holy Grail", "African Swallows"));
////
////        // Store the Document
////        bucket.upsert(JsonDocument.create("u:king_arthur", arthur));
////
////        // Load the Document and print it
////        // Prints Content and Metadata of the stored Document
////        System.out.println(bucket.get("u:king_arthur"));
////
////        // Create a N1QL Primary Index (but ignore if it exists)
////        bucket.bucketManager().createN1qlPrimaryIndex(true, false);
//
//        // Perform a N1QL Query
//        N1qlQueryResult result = bucket.query(
//            N1qlQuery.parameterized("SELECT name FROM `jarthur2` WHERE $1 IN interests",
//            JsonArray.from("African Swallows"))
//        );
//
//        // Print each found Row
//        for (N1qlQueryRow row : result) {
//            // Prints {"name":"Arthur"}
//            System.out.println(row);
//        }
//
//    }
//}
